<?php
namespace DAP;

use Httpful\Exception\ConnectionErrorException;
use Httpful\Request;
use DAP\ScanObject\Collection as ScanObjectCollection;
use DAP\ScanObject;
use DAP\Scan\UserList;
use DAP\Scan\UserData as PersonListItem;
use DAP\ScanModule;
use Httpful\Response;


class Client {

    const TESTING_ENDPOINT = 'http://private-anon-4e6a0af37-dapsapiv2.apiary-mock.com';

    const METHOD_POST = 'POST';
    const METHOD_GET = 'GET';
    const METHOD_PUT = 'PUT';
    const METHOD_DELETE = 'DELETE';

    protected static $client_data_map = [
        'name' => 1,
        'surname' => 2,
        'year_of_birth' =>3,
        'email' => 6,
        'gender' => 8,
        'age' => 10
    ];

    /**
     * @var string
     */
    protected $endpoint_URL;

    /**
     * @var string
     */
    protected $username = '';

    /**
     * @var string
     */
    protected $password = '';

    /**
     * @var array
     */
    protected $extra_request_headers = [];

    /**
     * @var string
     */
    protected $auth_token;

    /**
     * @var Request
     */
    protected $last_request;

    /**
     * @var Response
     */
    protected $last_response;

    /**
     * @param string $endpoint_URL
     * @param string $username [optional]
     * @param string $password [optional]
     */
    function __construct($endpoint_URL, $username = '', $password = '')
    {
        $this->endpoint_URL = rtrim($endpoint_URL, '/');
        $this->username = (string)$username;
        $this->password = (string)$password;
    }

    /**
     * @return array
     */
    public function getExtraRequestHeaders()
    {
        return $this->extra_request_headers;
    }

    /**
     * @param array $extra_request_headers
     */
    public function setExtraRequestHeaders(array $extra_request_headers)
    {
        $this->extra_request_headers = $extra_request_headers;
    }



    /**
     * @return string
     */
    public function getEndpointURL()
    {
        return $this->endpoint_URL;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @return bool
     */
    public function isAuthenticated()
    {
        return (bool)$this->auth_token;
    }

    /**
     * @param string $uri
     * @return string
     */
    public function getRequestURL($uri)
    {
        return $this->endpoint_URL . '/' . ltrim($uri, '/');
    }

    /**
     * Authenticate and get token
     *
     * @param bool|false $refresh_token [optional]
     * @return string
     * @throws Exception
     * @throws ConnectionErrorException
     */
    public function authenticate($refresh_token = false)
    {
        if($this->auth_token && !$refresh_token){
            return $this->auth_token;
        }

        $url = $this->getRequestURL('/account/login');
        $user = $this->getUsername();
        $pass = $this->getPassword();
        $headers = $this->getExtraRequestHeaders();

        $request = Request::post($url)->authenticateWith($user, $pass);
        if($headers){
            foreach($headers as $header => $value){
                $request->addHeader($header, $value);
            }
        }

        $this->last_request = $request;
        $this->last_response = null;
        try {

            /** @var \Httpful\Response $response */
            $response = $request->expects('json')->send();
            $this->last_response = $response;

        } catch(ConnectionErrorException $e){
            throw new Exception("DAP Connection error - {$e->getMessage()}", Exception::CODE_CONNECTION_ERROR, $e);
        }
        catch(\Exception $e){
            throw new Exception("DAP Unexpected error - {$e->getMessage()}", Exception::CODE_EXCEPTION, $e);
        }

        if(
            $response->code != 200 ||
            empty($response->body->token)
        ){
            throw new Exception("DAP - Failed to authenticate [code: {$response->code}]", Exception::CODE_HTTP_UNAUTHORIZED);
        }

        $this->auth_token = $response->body->token;
        return $this->auth_token;
    }

    /**
     * @param string $uri
     * @param null|mixed $json_data [opptional]
     * @param string $method [optional]
     * @param array $headers [optional]
     * @return \Httpful\Response
     * @throws Exception
     * @throws ConnectionErrorException
     */
    public function sendRequest($uri, $json_data = null, $method = self::METHOD_POST, array $headers = [])
    {
        $token = $this->authenticate();
        $request = Request::init($method, 'application/json');
        $url = $this->getRequestURL($uri);
        $request->uri($url);

        foreach($this->getExtraRequestHeaders() as $header => $value){
            if(!isset($headers[$header])){
                $headers[$header] = $value;
            }
        }

        $headers['Authorization'] = "AuthSub token=\"{$token}\"";
        if(!isset($headers['Accept'])){
            $headers['Accept'] = "application/json";
        }

        foreach($headers as $header => $value){
            $request->addHeader($header, $value);
        }

        if($json_data !== null){
            $request->body($json_data, 'json');
        }

        $this->last_request = $request;
        $this->last_response = null;
        try {

            /** @var \Httpful\Response $response */
            $response = $request->send();
            $this->last_response = $response;


        } catch(ConnectionErrorException $e){
            throw new Exception("DAP Connection error - {$e->getMessage()}", Exception::CODE_CONNECTION_ERROR, $e);
        } catch(\Exception $e){
            throw new Exception("DAP Unexpected error - {$e->getMessage()}", Exception::CODE_EXCEPTION, $e);
        }

        if($response->code != 200){
            $message = "DAP - Request failed - code {$response->code}";
            if(isset($response->body->error)){
                $error = $response->body->error;
                $message .= ", error {$error->error_code}: {$error->message}";
            }
            throw new Exception($message, $response->code);
        }

        return $response;
    }

    /**
     * @return Request
     */
    public function getLastRequest()
    {
        return $this->last_request;
    }

    /**
     * @return Response
     */
    public function getLastResponse()
    {
        return $this->last_response;
    }

    public function clearLastRequest()
    {
        $this->last_request = null;
    }

    public function clearLastResponse()
    {
        $this->last_response = null;
    }

    /**
     * @param ScanObjectCollection $collection
     * @throws Exception
     */
    function prepareScanCollection(ScanObjectCollection $collection)
    {
        if($collection->isPrepared()){
            return;
        }

        $response = $this->sendRequest('/scan_object/collection', $collection);

        /** @var ScanObject $object */
        foreach($collection as $idx => $object){
            if(isset($response->body[$idx]->id)){
                $object->setID($response->body[$idx]->id);
            }
        }

        if(!$collection->isPrepared()){
            throw new Exception("DAP - Some elements in collection have no ID", Exception::CODE_INVALID_RESPONSE);
        }
    }

    /**
     * @param Scan $scan
     * @throws Exception
     */
    public function saveScan(Scan $scan)
    {
        if($scan->hasScanCode()){
            $this->sendRequest("/scan/{$scan->getScanCode()}", $scan, self::METHOD_PUT);
            return;
        }

        $response = $this->sendRequest("/scan", $scan);
        if(!isset($response->body->id) || !isset($response->body->scan_code)){
            throw new Exception("DAP - Missing id or scan_code in save scan response", Exception::CODE_INVALID_RESPONSE);
        }

        $scan->setID($response->body->id);
        $scan->setScanCode($response->body->scan_code);
    }

    /**
     * @param string $scan_code
     * @return Scan|\DAP\Scan\MaskItem[]
     * @throws Exception
     */
    public function getScan($scan_code)
    {
        $response = $this->sendRequest("/scan/{$scan_code}", null, self::METHOD_GET);
        $body = $response->body;

        $scan = new Scan();
        $scan->setID($body->id);
        $scan->setScanCode($body->scan_code);
        $scan->setName($body->name);
        $scan->setDescription($body->description);
        $scan->setState($body->state);
        $scan->setScanAreaID($body->scan_area_id);
        $scan->setScanModuleID($body->scan_module_id);
        $scan->setModuleTypeID($body->module_type_id);

        $scan_mask = $scan->getScanMask();
        if(empty($body->scan_mask)){
            return $scan;
        }

        foreach($body->scan_mask as $item_data){
            $item = $scan_mask->addItem($item_data->label, $item_data->client_data_key);
            $item->setEditable($item_data->editable);
            $vars = get_object_vars($item_data);
            foreach($vars as $key => $value){
                if(in_array($key, ['label', 'editable', 'order', 'client_data_key'])){
                    continue;
                }
                $item->setConstraint($key, $value);
            }
        }

        return $scan;
    }

    /**
     * @param string $scan_code
     * @return UserList|\DAP\Scan\UserData[]
     * @throws Exception
     */
    public function getScanUsersData($scan_code)
    {
        $response = $this->sendRequest("/scan/{$scan_code}/scan_data", null, self::METHOD_GET);
        $body = $response->body;

        $list = new UserList();
        if(empty($body)){
            return $list;
        }

        foreach($body as $data){
            $item = new PersonListItem(
                $data->id,
                $data->module_type_id,
                $data->affected,
                !empty($data->client_data) ? get_object_vars($data->client_data) : []
            );
            $list->addUser($item);
        }

        return $list;
    }

    /**
     * @param \DAP\ScanModule $module
     * @throws Exception
     */
    public function createScanModule(ScanModule $module)
    {
        if($module->hasID()){
            throw new Exception("Module has already ID", Exception::CODE_INVALID_MODULE);
        }

        $response = $this->sendRequest('/scan_module', $module);
        if(!isset($response->body->scan_module)){
            throw new Exception("DAP - Missing id in create scan module response", Exception::CODE_INVALID_RESPONSE);
        }
        $module->setID($response->body->scan_module);
    }

    /**
     * @param int $module_ID
     * @return \DAP\ScanModule
     * @throws Exception
     */
    public function getScanModule($module_ID)
    {
        $module_ID = (int)$module_ID;
        $response = $this->sendRequest("/scan_module/{$module_ID}", null, self::METHOD_GET);
        $body = $response->body;
        $module = new ScanModule($body->name, $body->description, $body->id);
        foreach($body->scan_objects as $object){
            $scan_object = $module->addScanObject(
                $object->label,
                $object->object_type,
                $object->lang_code
            );
            $scan_object->setID($object->id);
        }
        return $module;
    }

    public function saveCollector(Collector $collector)
    {
        if($collector->hasID()){
            throw new Exception("Collector already has ID", Exception::CODE_INVALID_COLLECTOR);
        }

        $response = $this->sendRequest('/collector/get', $collector);
        if(!isset($response->body->id)){
            throw new Exception("DAP - Missing id in collector save response", Exception::CODE_INVALID_RESPONSE);
        }

        if(!isset($response->body->hash)){
            throw new Exception("DAP - Missing hash in collector save response", Exception::CODE_INVALID_RESPONSE);
        }

        $collector->setID($response->body->id);
        $collector->setHash($response->body->hash);
        $collector->setAffectation($response->body->affectation);

    }

    /**
     * @param array $parameters_ids
     * @param array $scan_data_ids
     * @return array
     * @throws Exception
     */
    public function computeParameters(array $parameters_ids, array $scan_data_ids)
    {
        if(empty($parameters_ids)){
            throw new Exception("No parameters specified", Exception::CODE_INVALID_ARGUMENT);
        }

        if(empty($scan_data_ids)){
            throw new Exception("No scan data IDs specified", Exception::CODE_INVALID_ARGUMENT);
        }

        $response = $this->sendRequest(
            '/parameters/compute',
            [
                'parameters_ids' => $parameters_ids,
                'scan_data_ids' => $scan_data_ids
            ]
        );

        if(empty($response->body)){
            throw new Exception("DAP - parameter values in response", Exception::CODE_INVALID_RESPONSE);
        }

        $params_array = json_decode(json_encode($response->body), true);
        return $params_array;
    }
}