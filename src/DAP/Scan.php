<?php
namespace DAP;

use DAP\Scan\Mask as ScanMask;

class Scan implements \JsonSerializable {

    const STATE_ACTIVE = 'ACTIVE';
    const STATE_CLOSED = 'CLOSED';
    const STATE_TEST = 'TEST';

    const AREA_SCHOOLS = 1;
    const AREA_COMMERCIAL = 3;

    const DEFAULT_SCAN_MODULE_ID = 1;

    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $scan_code;

    /**
     * @var string
     */
    protected $name = '';

    /**
     * @var string
     */
    protected $description = '';

    /**
     * @var string
     */
    protected $state;

    /**
     * @var int
     */
    protected $scan_area_id = self::AREA_COMMERCIAL;

    /**
     * @var int
     */
    protected $scan_module_id = self::DEFAULT_SCAN_MODULE_ID;

    /**
     * @var int
     */
    protected $module_type_id = 1;

    /**
     * @var ScanMask
     */
    protected $scan_mask;

    /**
     * @param string $state
     */
    function __construct($state = self::STATE_ACTIVE)
    {
        $this->setState($state);
        $this->scan_mask = new ScanMask();
    }

    /**
     * @return int|null
     */
    public function getID()
    {
        return $this->id;
    }

    /**
     * @param int $ID
     */
    public function setID($ID)
    {
        $this->id = (int)$ID;
    }

    /**
     * @return bool
     */
    public function hasID()
    {
        return $this->id !== null;
    }

    /**
     * @return string
     */
    public function getScanCode()
    {
        return $this->scan_code;
    }

    /**
     * @param string $scan_code
     */
    public function setScanCode($scan_code)
    {
        $this->scan_code = (string)$scan_code;
    }

    /**
     * @return bool
     */
    public function hasScanCode()
    {
        return $this->scan_code !== null;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = (string)$name;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = (string)$description;
    }

    /**
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @param string $state
     * @throws Exception
     */
    public function setState($state)
    {
        $valid_states = [self::STATE_ACTIVE, self::STATE_CLOSED, self::STATE_TEST];
        if(!in_array($state,$valid_states)){
            throw new Exception("Invalid states: " . implode(", ", $valid_states));
        }
        $this->state = $state;
    }

    /**
     * @return int
     */
    public function getScanAreaID()
    {
        return $this->scan_area_id;
    }

    /**
     * @param int $scan_area_ID
     */
    public function setScanAreaID($scan_area_ID)
    {
        $this->scan_area_id = (int)$scan_area_ID;
    }

    /**
     * @return int
     */
    public function getScanModuleID()
    {
        return $this->scan_module_id;
    }

    /**
     * @param int $scan_module_ID
     */
    public function setScanModuleID($scan_module_ID)
    {
        $this->scan_module_id = (int)$scan_module_ID;
    }

    /**
     * @return int
     */
    public function getModuleTypeID()
    {
        return $this->module_type_id;
    }

    /**
     * @param int $module_type_ID
     */
    public function setModuleTypeID($module_type_ID)
    {
        $this->module_type_id = (int)$module_type_ID;
    }

    /**
     * @return ScanMask
     */
    public function getScanMask()
    {
        return $this->scan_mask;
    }

    /**
     * @param string $label
     * @param int $client_data_key
     * @return Scan\MaskItem
     */
    public function addScanMaskItem($label, $client_data_key)
    {
        return $this->getScanMask()->addItem($label, $client_data_key);
    }

    /**
     * @return array
     */
    function jsonSerialize()
    {
        $props = get_object_vars($this);
        if(!$this->hasID()){
            unset($props['id']);
        }

        if(!$this->hasScanCode()){
            unset($props['scan_code']);
        }

        return $props;
    }
}