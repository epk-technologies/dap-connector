<?php
namespace DAP\ScanObject;

use DAP\ScanObject;

class Collection implements \Iterator, \JsonSerializable {

    /**
     * @var ScanObject[]
     */
    protected $objects = [];

    /**
     * @return bool
     */
    function isPrepared()
    {
        foreach($this->objects as $object){
            if(!$object->hasID()){
                return false;
            }
        }
        return true;
    }

    /**
     * @return ScanObject[]
     */
    public function getObjects()
    {
        return $this->objects;
    }

    /**
     * @param ScanObject[] $objects
     */
    public function setObjects($objects)
    {
        $this->objects = $objects;
    }

    /**
     * @param ScanObject $scan_object
     */
    public function add(ScanObject $scan_object)
    {
        $this->objects[] = $scan_object;
    }

    /**
     * @param int $ID
     */
    public function remove($ID)
    {
        $ID = (int)$ID;
        $new_objects = [];
        foreach($this->objects as $object){
            if($object->hasID() && $object->getID() === $ID){
                continue;
            }
            $new_objects[] = $object;
        }
        $this->objects = $new_objects;
    }

    /**
     * @param string $label
     * @param string|null $type [optional]
     */
    public function removeByLabel($label, $type = null)
    {
        $new_objects = [];
        foreach($this->objects as $idx => $object){
            if($object->getLabel() == $label){
                if(!$type || $type == $object->getObjectType()){
                    continue;
                }
            }
        }
        $this->objects = $new_objects;
    }

    /**
     * @param int $ID
     * @return bool|ScanObject
     */
    public function get($ID)
    {
        $ID = (int)$ID;
        foreach($this->objects as $object){
            if($object->hasID() && $object->getID() === $ID){
                return $object;
            }
        }
        return false;
    }

    /**
     * @param string $label
     * @param null|string $type [optional]
     * @return bool|ScanObject
     */
    public function getByLabel($label, $type = null)
    {
        foreach($this->objects as $idx => $object){
            if($object->getLabel() == $label){
                if(!$type || $type == $object->getObjectType()){
                    return $object;
                }
            }
        }
        return false;
    }

    /**
     * @param int $ID
     * @return bool
     */
    public function has($ID)
    {
        $ID = (int)$ID;
        foreach($this->objects as $object){
            if($object->hasID() && $object->getID() === $ID){
                return true;
            }
        }
        return false;
    }

    /**
     * @param string $label
     * @param null|string $type [optional]
     * @return bool
     */
    public function hasByLabel($label, $type = null)
    {
        foreach($this->objects as $idx => $object){
            if($object->getLabel() == $label){
                if(!$type || $type == $object->getObjectType()){
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * @return array
     */
    function jsonSerialize()
    {
        return $this->objects;
    }


    /**
     * @return ScanObject|false
     */
    public function current()
    {
        return current($this->objects);
    }

    public function next()
    {
        next($this->objects);
    }

    public function key()
    {
        return key($this->objects);
    }

    public function valid()
    {
        return key($this->objects) !== null;
    }

    public function rewind()
    {
        reset($this->objects);
    }
}