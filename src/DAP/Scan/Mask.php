<?php
namespace DAP\Scan;

class Mask implements \JsonSerializable, \Iterator {

    /**
     * @var MaskItem[]
     */
    protected $items = [];


    /**
     * @return MaskItem[]
     */
    public function getItems()
    {
        return $this->items;
    }

    /**
     * @param MaskItem[] $items
     */
    public function setItems(array $items)
    {
        $this->items = $items;
    }

    /**
     * @param string $label
     * @param int $client_data_key
     * @return MaskItem
     */
    public function addItem($label, $client_data_key)
    {
        $item = new MaskItem($label, $client_data_key);
        $item->setOrder(count($this->items) + 1);
        $this->items[$item->getClientDataKey()] = $item;
        return $item;
    }

    /**
     * @param int $client_data_key
     * @return MaskItem|null
     */
    public function getItem($client_data_key)
    {
        return isset($this->items[$client_data_key])
                ? $this->items[$client_data_key]
                : null;
    }

    /**
     * @param string $client_data_key
     * @return bool
     */
    public function hasItem($client_data_key)
    {
        return isset($this->items[$client_data_key]);
    }

    /**
     * @param int $client_data_key
     */
    public function removeItem($client_data_key)
    {
        if(isset($this->items[$client_data_key])){
            unset($this->items[$client_data_key]);
        }

        $counter = 1;
        foreach($this->items as $item){
            $item->setOrder($counter++);
        }
    }

    /**
     * @return MaskItem[]
     */
    function jsonSerialize()
    {
        return array_values($this->items);
    }

    /**
     * @return MaskItem|false
     */
    public function current()
    {
        return current($this->items);
    }


    public function next()
    {
        next($this->items);
    }

    /**
     * @return int|null
     */
    public function key()
    {
        return key($this->items);
    }

    /**
     * @return bool
     */
    public function valid()
    {
        return key($this->items) !== null;
    }


    public function rewind()
    {
        reset($this->items);
    }
}