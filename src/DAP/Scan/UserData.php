<?php
namespace DAP\Scan;

class UserData implements \JsonSerializable
{

    const KEY_NAME = 1; // Jméno
    const KEY_SURNAME = 2; // Příjmení
    const KEY_YEAR_OF_BIRTH = 3; // Datum narození (rok narození)
    const KEY_EDUCATION = 4; // Vzdělání
    const KEY_CITY = 5; // Obec
    const KEY_EMAIL = 6; // Email
    const KEY_REASON = 7; // Důvod snímání
    const KEY_GENDER = 8; // Pohlaví
    const KEY_GROUP = 9; // Skupina
    const KEY_AGE = 10; // Věk
    const KEY_JOB = 11; // Pracovní pozice
    const KEY_PERSON_ID = 12; // Id snímané osoby
    const KEY_RESPONSIBLE_PERSON_ID = 13; // Id zodpovědné osoby
    const KEY_COMPANY = 14; // Společnost
    const KEY_LANGUAGE = 15; // Language code - ISO 639-1
    const KEY_COUNTRY = 16; // Country code - ISO ALPHA-3 code
    const KEY_EXTERNAL_GROUP_ID = 17; // Externí Id skupiny snímků
    const KEY_CUSTOM = 18; // Volitelná položka od klienta

    const GENDER_MALE = 'MALE';
    const GENDER_FEMALE = 'FEMALE';

    /**
     * @var int
     */
    protected $id;

    /**
     * @var int
     */
    protected $module_type_id = 1;

    /**
     * @var bool
     */
    protected $affected = false;

    /**
     * @var array
     */
    protected $client_data = [];


    /**
     * @param int $ID
     * @param int $module_type_ID
     * @param bool $affected
     * @param array $client_data
     */
    function __construct($ID, $module_type_ID = 1, $affected = false, array $client_data = [])
    {
        $this->setID($ID);
        $this->setModuleTypeID($module_type_ID);
        $this->setAffected($affected);
        $this->setClientData($client_data);
    }


    /**
     * @return int|null
     */
    public function getID()
    {
        return $this->id;
    }

    /**
     * @param int $ID
     */
    public function setID($ID)
    {
        $this->id = (int)$ID;
    }

    /**
     * @return int
     */
    public function getModuleTypeID()
    {
        return $this->module_type_id;
    }

    /**
     * @param int $module_type_ID
     */
    public function setModuleTypeID($module_type_ID)
    {
        $this->module_type_id = (int)$module_type_ID;
    }

    /**
     * @return boolean
     */
    public function isAffected()
    {
        return $this->affected;
    }

    /**
     * @param boolean $affected
     */
    public function setAffected($affected)
    {
        $this->affected = (bool)$affected;
    }

    /**
     * @return array
     */
    public function getClientData()
    {
        return $this->client_data;
    }

    /**
     * @param int $key
     * @param null|mixed $default_value [optional]
     * @return mixed
     */
    public function getClientDataValue($key, $default_value = null)
    {
        return isset($this->client_data[$key])
                ? $this->client_data[$key]
                : $default_value;
    }

    /**
     * @param array $client_data
     */
    public function setClientData(array $client_data)
    {
        $this->client_data = $client_data;
    }

    /**
     * @return array
     */
    function jsonSerialize()
    {
        return get_object_vars($this);
    }
}