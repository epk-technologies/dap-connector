<?php
namespace DAP\Scan;

class MaskItem implements \JsonSerializable {

    const KEY_NAME = 1; // Jméno
    const KEY_SURNAME = 2; // Příjmení
    const KEY_YEAR_OF_BIRTH = 3; // Datum narození (rok narození)
    const KEY_EDUCATION = 4; // Vzdělání
    const KEY_CITY = 5; // Obec
    const KEY_EMAIL = 6; // Email
    const KEY_REASON = 7; // Důvod snímání
    const KEY_GENDER = 8; // Pohlaví
    const KEY_GROUP = 9; // Skupina
    const KEY_AGE = 10; // Věk
    const KEY_JOB = 11; // Pracovní pozice
    const KEY_PERSON_ID = 12; // Id snímané osoby
    const KEY_RESPONSIBLE_PERSON_ID = 13; // Id zodpovědné osoby
    const KEY_COMPANY = 14; // Společnost
    const KEY_LANGUAGE = 15; // Language code - ISO 639-1
    const KEY_COUNTRY = 16; // Country code - ISO ALPHA-3 code
    const KEY_EXTERNAL_GROUP_ID = 17; // Externí Id skupiny snímků
    const KEY_CUSTOM = 18; // Volitelná položka od klienta

    const GENDER_MALE = 'MALE';
    const GENDER_FEMALE = 'FEMALE';

    /**
     * @var string
     */
    protected $label;

    /**
     * @var bool
     */
    protected $editable = false;

    /**
     * @var int
     */
    protected $order;

    /**
     * @var int
     */
    protected $client_data_key;

    /**
     * @var array
     */
    protected $_constraints = [
        'number_from' => null,
        'number_to' => null,
        'number_step' => null,
        'list_items' => []
    ];

    /**
     * @param string $label
     * @param int $client_data_key
     */
    function __construct($label, $client_data_key)
    {
        $this->label = (string)$label;
        $this->client_data_key = (int)$client_data_key;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @return boolean
     */
    public function isEditable()
    {
        return $this->editable;
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @return bool
     */
    public function hasOrder()
    {
        return $this->order !== null;
    }

    /**
     * @return int
     */
    public function getClientDataKey()
    {
        return $this->client_data_key;
    }

    /**
     * @return array
     */
    public function getConstraints()
    {
        return $this->_constraints;
    }

    /**
     * @param boolean $editable
     */
    public function setEditable($editable)
    {
        $this->editable = (bool)$editable;
    }

    /**
     * @param int $order
     */
    public function setOrder($order)
    {
        $this->order = (int)$order;
    }

    /**
     * @param array $constraints
     */
    public function setConstraints(array $constraints)
    {
        foreach($constraints as $constraint => $value){
            $this->setConstraint($constraint, $value);
        }
    }

    /**
     * @param string $constraint
     * @param null|mixed $default_value [optional]
     * @return mixed
     */
    public function getConstraint($constraint, $default_value = null)
    {
        return isset($this->_constraints[$constraint])
                ? $this->_constraints[$constraint]
                : $default_value;
    }

    /**
     * @param string $constraint
     * @param mixed $value
     */
    public function setConstraint($constraint, $value)
    {
        $this->_constraints[$constraint] = $value;
    }


    /**
     * @return array
     */
    function jsonSerialize()
    {
        $props = get_object_vars($this);
        unset($props['_constraints']);
        foreach($this->_constraints as $constraint => $value){
            if($value === null || $value === []){
                continue;
            }
            $props[$constraint] = $value;
        }
        return $props;
    }
}