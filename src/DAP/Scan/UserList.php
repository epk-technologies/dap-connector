<?php
namespace DAP\Scan;

class UserList implements \JsonSerializable, \Iterator, \Countable
{
    /**
     * @var UserData[]
     */
    protected $users = [];

    /**
     * @return UserData[]
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * @param UserData[] $users
     */
    public function setUsers($users)
    {
        $this->users = $users;
    }

    /**
     * @param UserData $user
     */
    public function addUser(UserData $user)
    {
        $this->users[$user->getID()] = $user;
    }

    /**
     * @param int $ID
     * @return UserData|null
     */
    public function getUser($ID)
    {
        return isset($this->users[$ID])
                ? $this->users[$ID]
                : null;
    }

    /**
     * @param int $ID
     * @return bool
     */
    public function hasUser($ID)
    {
        return isset($this->users[$ID]);
    }

    /**
     * @param int $ID
     */
    public function removeUser($ID)
    {
        if(isset($this->users[$ID])){
            unset($this->users[$ID]);
        }
    }

    /**
     * @return array
     */
    function jsonSerialize()
    {
        return array_values($this->users);
    }

    /**
     * @return UserData|false
     */
    public function current()
    {
        return current($this->users);
    }


    public function next()
    {
        next($this->users);
    }

    /**
     * @return int|null
     */
    public function key()
    {
        return key($this->users);
    }

    /**
     * @return bool
     */
    public function valid()
    {
        return key($this->users) !== null;
    }


    public function rewind()
    {
        reset($this->users);
    }

    /**
     * @return int
     */
    public function count()
    {
        return count($this->users);
    }
}