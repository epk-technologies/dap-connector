<?php
namespace DAP\ScanModule;

class Item implements \JsonSerializable {

    const DEFAULT_LANGUAGE = 'cs';

    const TYPE_WORD = 'WORD';
    const TYPE_PICTURE = 'PICTURE';
    const TYPE_VIDEO = 'VIDEO';
    const TYPE_AUDIO = 'AUDIO';

    /**
     * @var int
     */
    protected $id;

    /**
     * @var string
     */
    protected $label;

    /**
     * @var string
     */
    protected $object_type = self::TYPE_WORD;

    /**
     * @var string
     */
    protected $lang_code = self::DEFAULT_LANGUAGE;

    /**
     * @var int
     */
    protected $order;

    /**
     * @param string $label
     * @param string $object_type
     * @param string $lang_code
     * @param int|null $ID [optional]
     */
    public function __construct($label, $object_type = self::TYPE_WORD, $lang_code = self::DEFAULT_LANGUAGE, $ID = null)
    {
        $this->setLabel($label);
        $this->setObjectType($object_type);
        $this->setLangCode($lang_code);
        if($ID !== null){
            $this->setID($ID);
        }
    }

    /**
     * @param int $ID
     */
    public function setID($ID)
    {
        $this->id = (int)$ID;
    }

    /**
     * @return mixed
     */
    public function getID()
    {
        return $this->id;
    }

    public function hasID()
    {
        return $this->id !== null;
    }

    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @param string $label
     */
    public function setLabel($label)
    {
        $this->label = (string)$label;
    }

    /**
     * @return string
     */
    public function getObjectType()
    {
        return $this->object_type;
    }

    /**
     * @param string $object_type
     */
    public function setObjectType($object_type)
    {
        $this->object_type = (string)$object_type;
    }

    /**
     * @return string
     */
    public function getLangCode()
    {
        return $this->lang_code;
    }

    /**
     * @param string $lang_code
     */
    public function setLangCode($lang_code)
    {
        $this->lang_code = (string)$lang_code;
    }

    /**
     * @return int
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * @param int $order
     */
    public function setOrder($order)
    {
        $this->order = (int)$order;
    }

    /**
     * @return array
     */
    function jsonSerialize()
    {
        $props = get_object_vars($this);
        if(!$this->hasID()){
            unset($props['id']);
        }
        return $props;
    }
}