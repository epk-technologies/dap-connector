<?php
namespace DAP;

use DAP\ScanModule\Item;

class ScanModule implements \JsonSerializable, \Iterator, \Countable {

    /**
     * @var int|null
     */
    protected $id;

    /**
     * @var string
     */
    protected $name;

    /**
     * @var string
     */
    protected $description = '';

    /**
     * @var Item[]
     */
    protected $scan_objects = [];


    /**
     * @param string $name
     * @param string $description [optional]
     * @param null|int $id [optional]
     */
    function __construct($name, $description = '', $id = null)
    {
        $this->name = trim($name);
        $this->description = trim($description);
        if($id !== null){
            $this->setID($id);
        }
    }

    /**
     * @param int $ID
     */
    public function setID($ID)
    {
        $this->id = (int)$ID;
    }

    /**
     * @return mixed
     */
    public function getID()
    {
        return $this->id;
    }

    public function hasID()
    {
        return $this->id !== null;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return ScanModule\Item[]
     */
    public function getScanObjects()
    {
        return $this->scan_objects;
    }

    /**
     * @param string $label
     * @param string $type [optional]
     * @param string $language [optional]
     * @return Item
     */
    public function addScanObject($label, $type = Item::TYPE_WORD, $language = Item::DEFAULT_LANGUAGE)
    {
        $item = new Item($label, $type, $language);
        $item->setOrder(count($this->scan_objects) + 1);
        $this->scan_objects[] = $item;
        return $item;
    }

    /**
     * @param string $label
     * @param string|null $type [optional]
     * @return bool
     */
    public function hasScanObject($label, $type = null)
    {
        foreach($this->scan_objects as $object){
            if(
                $object->getLabel() === (string)$label &&
                (
                    $type === null ||
                    $object->getObjectType() == $type
                )
            ) {
                return true;
            }
        }
        return false;
    }

    /**
     * @param int $index
     * @return Item|null
     */
    public function getScanObjectByIndex($index)
    {
       return isset($this->scan_objects[$index])
             ? $this->scan_objects[$index]
             : null;
    }

    /**
     * @param int $ID
     * @return Item|null
     */
    public function getScanObjectByID($ID)
    {
        foreach($this->scan_objects as $object){
            if($object->hasID() && $object->getID() == $ID){
                return $object;
            }
        }
        return null;
    }

    /**
     * @param int $order
     * @return Item|null
     */
    public function getScanObjectByOrder($order)
    {
        foreach($this->scan_objects as $object){
            if($object->getOrder() == $order){
                return $object;
            }
        }
        return null;
    }

    /**
     * @param string $label
     * @param string|null $type [optional]
     * @return Item|null
     */
    public function getScanObjectByLabel($label, $type = null)
    {
        foreach($this->scan_objects as $object){
            if(
                $object->getLabel() === (string)$label &&
                (
                    $type === null ||
                    $object->getObjectType() == $type
                )
            ) {
                return $object;
            }
        }
        return null;
    }

    /**
     * @param int $index
     */
    public function removeScanObjectByIndex($index)
    {
        if(!isset($this->scan_objects[$index])){
            return;
        }
        unset($this->scan_objects[$index]);
        $this->scan_objects = array_values($this->scan_objects);

        foreach($this->scan_objects as $idx => $object){
            $object->setOrder($idx + 1);
        }
    }

    /**
     * @param int $ID
     */
    public function removeScanObjectByID($ID)
    {
        foreach($this->scan_objects as $idx => $object){
            if($object->hasID() && $object->getID() == $ID) {
                $this->removeScanObjectByIndex($idx);
                break;
            }
        }
    }


    /**
     * @param string $label
     * @param string|null $type [optional]
     */
    public function removeScanObjectByLabel($label, $type = null)
    {
        foreach($this->scan_objects as $idx => $object){
            if(
                $object->getLabel() === (string)$label &&
                (
                    $type === null ||
                    $object->getObjectType() == $type
                )
            ) {
                $this->removeScanObjectByIndex($idx);
                break;
            }
        }
    }


    /**
     * @return array
     */
    function jsonSerialize()
    {
        $data = get_object_vars($this);
        if($this->id === null){
            unset($data['id']);
        }
        return $data;
    }

    /**
     * @return Item|false
     */
    public function current()
    {
        return current($this->scan_objects);
    }


    public function next()
    {
        next($this->scan_objects);
    }

    /**
     * @return int|null
     */
    public function key()
    {
        return key($this->scan_objects);
    }

    /**
     * @return bool
     */
    public function valid()
    {
        return key($this->scan_objects) !== null;
    }


    public function rewind()
    {
        reset($this->scan_objects);
    }

    /**
     * @return int
     */
    public function count()
    {
        return count($this->scan_objects);
    }
}