<?php

namespace DAP\Collector\SelectionGroup\Selection;

class Choice implements \JsonSerializable
{
    /**
     * @var int
     */
    protected $color_id;

    /**
     * @var string
     */
    protected $selection_time;


    function __construct($color_id, $selection_time = null)
    {
        $this->setColorId($color_id);

        if(!$selection_time){
            $this->selection_time = date(DATE_ISO8601);
        } else {
            $this->selection_time = $selection_time;
        }
    }

    /**
     * @return int
     */
    public function getColorId()
    {
        return $this->color_id;
    }

    /**
     * @param int $color_id
     */
    public function setColorId($color_id)
    {
        $this->color_id = (int)$color_id;
    }

    /**
     * @return string
     */
    public function getSelectionTime()
    {
        return $this->selection_time;
    }

    /**
     * @param string $selection_time
     */
    public function setSelectionTime($selection_time)
    {
        $this->selection_time = $selection_time;
    }


    /**
     * @return array
     */
    function jsonSerialize()
    {
        $data = get_object_vars($this);

        return $data;
    }
}