<?php

namespace DAP\Collector\SelectionGroup;

use DAP\Collector\SelectionGroup\Selection\Choice;

class Selection implements \JsonSerializable
{
    /**
     * @var string
     */
    protected $begin_time;

    /**
     * @var string
     */
    protected $end_time;

    /**
     * @var Choice[]
     */
    protected $choices = [];

    /**
     * @return string
     */
    public function getBeginTime()
    {
        return $this->begin_time;
    }

    /**
     * @param string $begin_time
     */
    public function setBeginTime($begin_time)
    {
        $this->begin_time = $begin_time;
    }

    /**
     * @return string
     */
    public function getEndTime()
    {
        return $this->end_time;
    }

    /**
     * @param string $end_time
     */
    public function setEndTime($end_time)
    {
        $this->end_time = $end_time;
    }

    /**
     * @return Choice[]
     */
    public function getChoices()
    {
        return $this->choices;
    }

    /**
     * @param Choice[] $choices
     */
    public function setChoices($choices)
    {
        $this->choices = $choices;
    }

    /**
     * @param Choice $choice
     */
    public function addChoice(Choice $choice)
    {
        $this->choices[] = $choice;
    }

    /**
     * @return array
     */
    function jsonSerialize()
    {
        $data = get_object_vars($this);

        return $data;
    }
}