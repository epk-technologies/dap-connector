<?php

namespace DAP\Collector;

use DAP\Collector\SelectionGroup\Selection;

class SelectionGroup implements \JsonSerializable
{
    /**
     * @var int
     */
    protected $scan_object_id;

    /**
     * @var Selection
     */
    protected $selections;

    /**
     * @return int
     */
    public function getScanObjectId()
    {
        return $this->scan_object_id;
    }

    /**
     * @param int $scan_object_id
     */
    public function setScanObjectId($scan_object_id)
    {
        $this->scan_object_id = (int)$scan_object_id;
    }

    /**
     * @return Selection
     */
    public function getSelections()
    {
        return $this->selections;
    }

    /**
     * @param Selection $selections
     */
    public function setSelections(Selection $selections)
    {
        $this->selections = $selections;
    }
    

    /**
     * @return array
     */
    function jsonSerialize()
    {
        $data = get_object_vars($this);

        return $data;
    }
}