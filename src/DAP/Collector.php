<?php

namespace DAP;

use DAP\Collector\SelectionGroup;

class Collector implements \JsonSerializable
{
    /**
     * @var int|null
     */
    protected $id;

    /**
     * @var string
     */
    protected $hash;

    /**
     * @var bool
     */
    protected $affectation;

    /**
     * @var array
     */
    protected $client_data = [];

    /**
     * @var int
     */
    protected $scan_id;

    /**
     * @var string
     */
    protected $begin_time;

    /**
     * @var string
     */
    protected $end_time;

    /**
     * @var int
     */
    protected $module_type_id;

    /**
     * @var SelectionGroup[]
     */
    protected $selections = [];

    /**
     * @return int|null
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int|null $id
     */
    public function setId($id)
    {
        $this->id = (int)$id;
    }

    /**
     * @return bool
     */
    public function hasID()
    {
        return $this->id !== null;
    }

    /**
     * @return string
     */
    public function getHash()
    {
        return $this->hash;
    }

    /**
     * @param string $hash
     */
    public function setHash($hash)
    {
        $this->hash = $hash;
    }

    /**
     * @return array
     */
    public function getClientData()
    {
        return $this->client_data;
    }

    /**
     * @param array $client_data
     */
    public function setClientData($client_data)
    {
        $this->client_data = $client_data;
    }

    /**
     * @param string $key
     * @param mixed $value
     */
    public function addClientData($key, $value)
    {
        $this->client_data[] = [
            "client_data_id" => $key,
            "value" => $value
        ];
    }

    /**
     * @return int
     */
    public function getScanId()
    {
        return $this->scan_id;
    }

    /**
     * @param int $scan_id
     */
    public function setScanId($scan_id)
    {
        $this->scan_id = (int)$scan_id;
    }

    /**
     * @return string
     */
    public function getBeginTime()
    {
        return $this->begin_time;
    }

    /**
     * @param string $begin_time
     */
    public function setBeginTime($begin_time)
    {
        $this->begin_time = $begin_time;
    }

    /**
     * @return string
     */
    public function getEndTime()
    {
        return $this->end_time;
    }

    /**
     * @param string $end_time
     */
    public function setEndTime($end_time)
    {
        $this->end_time = $end_time;
    }

    /**
     * @return int
     */
    public function getModuleTypeId()
    {
        return $this->module_type_id;
    }

    /**
     * @param int $module_type_id
     */
    public function setModuleTypeId($module_type_id)
    {
        $this->module_type_id = $module_type_id;
    }

    /**
     * @return SelectionGroup[]
     */
    public function getSelections()
    {
        return $this->selections;
    }

    /**
     * @param SelectionGroup[] $selections
     */
    public function setSelections($selections)
    {
        $this->selections = $selections;
    }

    /**
     * @param SelectionGroup $selection
     */
    public function addSelection($selection)
    {
        $this->selections[] = $selection;
    }

    /**
     * @return boolean
     */
    public function isAffectation()
    {
        return $this->affectation;
    }

    /**
     * @param boolean $affectation
     */
    public function setAffectation($affectation)
    {
        $this->affectation = (bool)$affectation;
    }



    /**
     * @return array
     */
    function jsonSerialize()
    {
        $data = get_object_vars($this);
        if($this->id === null){
            unset($data['id']);
        }

        if($this->hash === null){
            unset($data['hash']);
        }

        if($this->affectation === null){
            unset($data['affectation']);
        }

        return $data;
    }
}