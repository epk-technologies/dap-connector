<?php
namespace DAP;

class Exception extends \Exception {

    const CODE_HTTP_UNAUTHORIZED = 401;
    const CODE_HTTP_BAD_REQUEST = 400;

    const CODE_CONNECTION_ERROR = 1000;
    const CODE_EXCEPTION = 1100;
    const CODE_INVALID_RESPONSE = 1600;
    const CODE_INVALID_MODULE = 1700;
    const CODE_INVALID_COLLECTOR = 1800;
    const CODE_INVALID_ARGUMENT = 1900;
}