<?php
namespace DAP;


class ScanObject implements \JsonSerializable {

    const DEFAULT_LANGUAGE = 'cs';

    const TYPE_WORD = 'WORD';
    const TYPE_PICTURE = 'PICTURE';
    const TYPE_VIDEO = 'VIDEO';
    const TYPE_AUDIO = 'AUDIO';

    protected $id;
    protected $label;
    protected $object_type = self::TYPE_WORD;
    protected $lang_code = self::DEFAULT_LANGUAGE;

    /**
     * @param string $label
     * @param string $type [optional]
     * @param string $language [optional]
     * @param null|int $id [optional]
     */
    function __construct($label, $type = self::TYPE_WORD, $language = self::DEFAULT_LANGUAGE, $id = null)
    {
        $this->label = trim($label);
        $this->object_type = trim($type);
        $this->lang_code = trim($language);
        if($id !== null){
            $this->setID($id);
        }
    }

    /**
     * @param int $ID
     */
    public function setID($ID)
    {
        $this->id = (int)$ID;
    }

    /**
     * @return mixed
     */
    public function getID()
    {
        return $this->id;
    }

    public function hasID()
    {
        return $this->id !== null;
    }

    /**
     * @return mixed
     */
    public function getLabel()
    {
        return $this->label;
    }

    /**
     * @return string
     */
    public function getObjectType()
    {
        return $this->object_type;
    }

    /**
     * @return string
     */
    public function getLangCode()
    {
        return $this->lang_code;
    }


    /**
     * @param string $word
     * @param string $language [optional]
     * @return ScanObject
     */
    public static function createWord($word, $language = self::DEFAULT_LANGUAGE)
    {
        return new static($word, self::TYPE_WORD, $language);
    }

    /**
     * @param string $picture
     * @param string $language [optional]
     * @return ScanObject
     */
    public static function createPicture($picture, $language = self::DEFAULT_LANGUAGE)
    {
        return new static($picture, self::TYPE_PICTURE, $language);
    }

    /**
     * @param string $video
     * @param string $language [optional]
     * @return ScanObject
     */
    public static function createVideo($video, $language = self::DEFAULT_LANGUAGE)
    {
        return new static($video, self::TYPE_VIDEO, $language);
    }

    /**
     * @param string $audio
     * @param string $language [optional]
     * @return ScanObject
     */
    public static function createAudio($audio, $language = self::DEFAULT_LANGUAGE)
    {
        return new static($audio, self::TYPE_AUDIO, $language);
    }

    /**
     * @return array
     */
    function jsonSerialize()
    {
        $data = get_object_vars($this);
        if($this->id === null){
            unset($data['id']);
        }
        return $data;
    }
}