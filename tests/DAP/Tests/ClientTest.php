<?php
namespace DAP\Tests;

use DAP\Client;
use DAP\Collector;
use DAP\ScanObject\Collection as ScanObjectCollection;
use DAP\ScanObject;
use DAP\Scan;
use DAP\Scan\MaskItem as ScanMaskItem;
use DAP\ScanModule;


class ClientTest extends \PHPUnit_Framework_TestCase {

    /**
     * @var Client
     */
    protected $client;

    function setUp()
    {
        $this->client = new Client(Client::TESTING_ENDPOINT);
    }

    function testAuthenticate()
    {
        $token = $this->client->authenticate();
        $this->assertEquals('40287ee649518e890149577d1524000sda2112123', $token);
    }

    /**
     * @expectedException \DAP\Exception
     * @expectedExceptionCode \DAP\Exception::CODE_HTTP_UNAUTHORIZED
     * @expectedExceptionMessage DAP - Failed to authenticate [code: 401]
     */
    function testAuthenticationFailed()
    {
        $this->client->setExtraRequestHeaders(['Prefer' => 'status=401']);
        $this->client->authenticate();
    }

    function testPrepareCollection()
    {
        $collection = new ScanObjectCollection();
        $collection->add(ScanObject::createWord('IT', 'en'));
        $collection->add(ScanObject::createWord('Projekt', 'en'));
        $this->client->prepareScanCollection($collection);

        $this->assertNotEmpty($collection->get(1));
        $this->assertEquals('IT', $collection->get(1)->getLabel());

        $this->assertNotEmpty($collection->get(2));
        $this->assertEquals('Projekt', $collection->get(2)->getLabel());
    }

    /**
     * @expectedException \DAP\Exception
     * @expectedExceptionCode \DAP\Exception::CODE_HTTP_BAD_REQUEST
     */
    function testPrepareWrongCollection()
    {
        $this->client->authenticate();
        $this->client->setExtraRequestHeaders(['Prefer' => 'status=400']);
        $collection = new ScanObjectCollection();
        $collection->add(new ScanObject("Test", "WRONG"));
        $this->client->prepareScanCollection($collection);
    }

    /**
     * @expectedException \DAP\Exception
     * @expectedExceptionCode \DAP\Exception::CODE_HTTP_UNAUTHORIZED
     */
    function testPrepareCollectionAuthFailed()
    {
        $this->client->authenticate();
        $this->client->setExtraRequestHeaders(['Prefer' => 'status=401']);
        $collection = new ScanObjectCollection();
        $collection->add(new ScanObject("Test", "WORD"));
        $this->client->prepareScanCollection($collection);
    }

    function testSaveScan()
    {
        $scan = new Scan();
        $scan->setName('Test scan');
        $scan->setDescription('Test scan description');
        $scan->addScanMaskItem("Name", ScanMaskItem::KEY_NAME);

        $this->client->saveScan($scan);
        $this->assertEquals(1, $scan->getID());
        $this->assertEquals('scan_code', $scan->getScanCode());

        $this->client->saveScan($scan);
    }

    function testGetScan()
    {
        $scan = $this->client->getScan('scan_code');
        $this->assertEquals(1, $scan->getID());
        $this->assertEquals('scan_code', $scan->getScanCode());
        $this->assertEquals('Scan name', $scan->getName());
        $this->assertEquals('Description', $scan->getDescription());
        $this->assertEquals('ACTIVE', $scan->getState());
        $this->assertEquals(1, $scan->getScanAreaID());
        $this->assertEquals(1, $scan->getScanModuleID());
        $this->assertEquals(1, $scan->getModuleTypeID());

        $scan_item = new ScanMaskItem('Gender', ScanMaskItem::KEY_GENDER);
        $scan_item->setOrder(1);
        $scan_item->setConstraint('list_items', ["1", "2"]);
        $this->assertEquals($scan_item, $scan->getScanMask()->getItem(ScanMaskItem::KEY_GENDER));

        $scan_item = new ScanMaskItem('Name', ScanMaskItem::KEY_NAME);
        $scan_item->setOrder(2);
        $scan_item->setEditable(true);
        $this->assertEquals($scan_item, $scan->getScanMask()->getItem(ScanMaskItem::KEY_GENDER));
    }

    function testGetScanUsersData()
    {
        $list = $this->client->getScanUsersData('scan_code');
        $this->assertEquals(9, count($list));

        $item = $list->getUser(9);
        $this->assertInstanceOf("DAP\\Scan\\UserData", $item);
        $this->assertEquals(9, $item->getID());
        $this->assertEquals(1, $item->getModuleTypeID());
        $this->assertEquals(false, $item->isAffected());

        $values = [
            '8' => 'MALE',
            '1' => 'Daniel',
            '2' => 'Architect',
            '3' => '1986',
            '9' => 'Department 2',
            '18' => 'Item 1',
            '10' => '26'
        ];


        foreach($values as $key => $expected){
            $this->assertEquals($expected, $item->getClientDataValue($key), $key);
        }
    }

    function testCreateScanModule()
    {
        $module = new ScanModule('Test', 'Test');
        $module->addScanObject('Test word');
        $this->client->createScanModule($module);
        $this->assertEquals(100, $module->getID());
    }

    function testGetScanModule()
    {
        $module = $this->client->getScanModule(558);
        $this->assertEquals(558, $module->getID());
        $this->assertEquals("Sportimind - Energie CZ", $module->getName());
        $this->assertEquals('Sportimind - Energie CZ', $module->getDescription());
        $this->assertEquals(3, count($module->getScanObjects()));

        $object = $module->getScanObjectByID(21271);
        $this->assertInstanceOf("DAP\\ScanModule\\Item", $object);
        $this->assertEquals('Moje psychika', $object->getLabel());
        $this->assertEquals(21271, $object->getID());
        $this->assertEquals(3, $object->getOrder());
        $this->assertEquals('WORD', $object->getObjectType());
        $this->assertEquals('cs', $object->getLangCode());
    }

    function testSaveCollector()
    {
        $collector = new Collector();
        $collector->addClientData(Scan\UserData::KEY_GENDER, Scan\UserData::GENDER_MALE);
        $collector->addClientData(Scan\UserData::KEY_AGE, 33);
        $collector->setScanId(123456);
        $collector->setBeginTime(date(DATE_ISO8601, strtotime("2013-07-01T10:00:00.125-04:00")));
        $collector->setEndTime(date(DATE_ISO8601, strtotime("2013-07-01T10:05:00.125-04:00")));
        $collector->setModuleTypeId(1);

        $selection = new Collector\SelectionGroup\Selection();
        $selection->setBeginTime(date(DATE_ISO8601, strtotime("2013-07-01T10:00:10.125-04:00")));
        $selection->setBeginTime(date(DATE_ISO8601, strtotime("2013-07-01T10:01:00.125-04:00")));

        $selection->addChoice(new Collector\SelectionGroup\Selection\Choice(1, date(DATE_ISO8601, strtotime("2013-07-01T10:00:20.125-04:00"))));
        $selection->addChoice(new Collector\SelectionGroup\Selection\Choice(2, date(DATE_ISO8601, strtotime("2013-07-01T10:00:30.125-04:00"))));
        $selection->addChoice(new Collector\SelectionGroup\Selection\Choice(3, date(DATE_ISO8601, strtotime("2013-07-01T10:00:40.125-04:00"))));

        $selection_group = new Collector\SelectionGroup();
        $selection_group->setScanObjectId(1234);
        $selection_group->addSelection($selection);

        $collector->addSelection($selection_group);

        $this->client->saveCollector($collector);

        $this->assertNotEmpty($collector->getID());
        $this->assertNotEmpty($collector->getHash());
    }
}